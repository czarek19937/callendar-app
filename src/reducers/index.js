/**
 * Created by cburas on 02.05.2017.
 */
import calendar from './CalendarReducer'
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    calendarEvents: calendar
});

export default rootReducer;