/**
 * Created by cburas on 02.05.2017.
 */
import React, { Component } from 'react';

import { connect } from 'react-redux';

class Button extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return <button onClick={this.props.onClickEvent}>{this.props.content}</button>;
    }
}

const mapStateToProps = (state) => {
    return {
        test: "test1"
    }
}

export default connect(mapStateToProps)(Button)