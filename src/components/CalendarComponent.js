/**
 * Created by cburas on 02.05.2017.
 */
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

import 'react-big-calendar/lib/css/react-big-calendar.css'
import 'react-big-calendar/lib/addons/dragAndDrop/styles.less';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';

import Button from './ButtonComponent'
import '../App.css';
import * as calendarActions from '../actions/CalendarActions';

const DragAndDropCalendar = withDragAndDrop(BigCalendar);
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer

function Event({ event }) {
    return (
        <span>
      <strong>
      {event.title}
      </strong>
            { event.desc && (':  ' + event.desc)}
    </span>
    )
}

function EventAgenda({ event }) {
    return <span>
    <em style={{ color: 'magenta'}}>{event.title}</em>
    <p>{ event.desc }</p>
  </span>
}

export class Calendar extends React.Component {
    constructor(props) {
        super(props);

        this.moveEvent = this.moveEvent.bind(this)
        this.addEvent = this.addEvent.bind(this)
    }

    moveEvent({ event, start, end }) {
        const events = this.props.calendarEvents;
        const idx = events.indexOf(event);
        const updatedEvent = { ...event, start, end };
        const nextEvents = [...events]

        nextEvents.splice(idx, 1, updatedEvent)
        this.props.actions.editCalendarEvents(nextEvents)

        alert(`${event.title} was dropped onto ${event.start}`);
    }

    addEvent(e){
        console.log(e)
    }


    render() {
        return (
            <div className="calendar-container">
                <Button onClickEvent={this.addEvent} content="Dodaj event"/>
                <DragAndDropCalendar
                    selectable
                    popup
                    timeslots={2}
                    events={this.props.calendarEvents}
                    onEventDrop={this.moveEvent}
                    defaultView='week'
                    scrollToTime={new Date(1970, 1, 1, 6)}
                    defaultDate={new Date(2017, 3, 12)}
                    onSelectEvent={event => console.log(event.title)}
                    onSelectSlot={(slotInfo) => console.log(
                        `selected slot: \n\nstart ${slotInfo.start.toLocaleString()} ` +
                        `\nend: ${slotInfo.end.toLocaleString()}`
                    )}
                    components={{
                        event: Event,
                        agenda: {
                            event: EventAgenda
                        }
                    }}
                />

            </div>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        calendarEvents: state.calendarEvents
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(calendarActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DragDropContext(HTML5Backend)(Calendar));