/**
 * Created by cburas on 20.04.2017.
 */
export const ActionTypes = {
    ADD: 'ADD',
    SHOW_EVENTS: 'SHOW_EVENTS',
    ADD_EVENT: 'ADD_EVENT',
    EDIT_EVENTS: 'EDIT_EVENTS'
};

export default ActionTypes