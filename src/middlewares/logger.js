/**
 * Created by cburas on 03.05.2017.
 */
export const logger = store => next => action => {
    console.log('dispatching', action)
    let result = next(action)
    console.log('next state', store.getState())
    return result
}

export default logger;