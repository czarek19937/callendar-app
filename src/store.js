/**
 * Created by cburas on 02.05.2017.
 */
import { createStore, applyMiddleware, compose } from 'redux'
import logger from './middlewares/logger'
import rootReducer from  './reducers';

export default(initialState) => {
    return createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(logger),
            window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
        )
    )
}