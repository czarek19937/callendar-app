/**
 * Created by cburas on 02.05.2017.
 */
import ActionTypes from '../ActionTypes'

export const addToCalendarEvents = (event) => {
    return {
        type: ActionTypes.ADD_EVENT,
        payload: {
            event: event
        }
    };
}

export const editCalendarEvents = (event) => {
    return {
        type: ActionTypes.EDIT_EVENTS,
        payload: {
            event: event
        }
    };
}